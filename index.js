// Bài 6.1: Tìm số nguyên dương nhỏ nhất
var sum = 0;
for (i = 1; 10000 - sum > 0; i++) {
  sum += i;
  //   console.log("sum = ", sum);
  if (10000 - sum < 0) {
    put_text_into_id("alert1", `n = ${i}`);
    break;
  }
}
//Bài 6.2: Tính tổng
function calculate_sum() {
  var S = 0;
  x = dom_id_value("x");
  n = dom_id_value("n");
  if (x != "" && n != "" && x > 0 && n > 0) {
    for (i = 1; i <= n; i++) {
      S += Math.pow(x, i);
    }
    put_text_into_id("alert2", `${S}`);
    return_value("x", 0);
    return_value("n", 0);
  } else {
    put_text_into_id("alert2", `Vui lòng nhập!`);
  }
}

// Bài 6.3: Tính giai thừa
var factorial = 1;
function calculate_factorial() {
  n = dom_id_value("input_number");
  if (n != "" && n > 0) {
    for (i = 1; i <= n; i++) {
      factorial *= i;
    }
    put_text_into_id("alert3", `${factorial}`);
    return (factorial = 1), return_value("input_number", "");
  } else {
    put_text_into_id("alert3", `Vui lòng nhập!`);
    return_value("input_number", "");
  }
}

//Bài 6.4: In thẻ div
function enter_to_print() {
  var input = dom_id("container_6_1");
  input.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      print_div();
    }
  });
}

function print_div() {
  for (i = 1; i <= 10; i++) {
    //tạo thẻ mới sau đó mới thêm vào thẻ div cha
    var tag_div = document.createElement("div");
    var tag_p = document.createElement("p");
    tag_p.style.cssText = "color: white; line-height: 45px;";
    tag_div.style.cssText = "width: 400px; height: 45px; margin-bottom: 5px";
    if (i % 2 != 0) {
      tag_div.style.backgroundColor = "#0000ffa8";
      tag_p.innerHTML = `div lẻ ${i}`;
      tag_div.appendChild(tag_p);
    } else {
      tag_div.style.backgroundColor = "#ff00009e";
      tag_p.innerHTML = `div chẵn ${i}`;
      tag_div.appendChild(tag_p);
    }
    dom_id("div_6_4").appendChild(tag_div);
  }
}
